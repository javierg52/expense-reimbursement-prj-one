# Expense Reimbursement Prj One

## Project Description
This system allows for quick and effecient handling and viewing of expense reimbursement requests. Managers are able to view any submitted requests, and can filter those requests by a single employee or by pending status. Managers can also approve or deny any pending requests. Employees are able to submit new requests, and they are able to see the status of all of their previous requests.

## Technologies Used
* Java 1.8
* AWS RDB
* PostgreSQL
* Hibernate
* Javalin
* HTML/ CSS/ JavaScript

## Features
* Managers and Employees can log in
* Employees can submit expense reimbursement requests and review them
* Managers can view and approve or deny expense reimbursement requests
* Managers can view all of their employees
