package com.revature.tests;

import com.revature.controllers.EmployeeController;
import com.revature.controllers.LoginController;
import com.revature.controllers.ReimbursementController;
import com.revature.data.EmployeeRepo;
import com.revature.data.ReimbursementRepo;
import com.revature.models.Employee;
import com.revature.models.Reimbursement;
import io.javalin.http.Context;
import org.junit.*;
import com.revature.utilities.BCryptHasher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;

import javax.security.auth.login.LoginContext;
import java.util.List;

import static org.mockito.Mockito.mock;

public class UnitTests {

//    @Mock
//    EmployeeController ec;
//
//    @Mock
//    LoginController lc;
//
//    @Mock
//    ReimbursementController rc;

    //Configuration methods
    @BeforeClass
    public static void init() {

    }

    @Before
    public void setup() {
    }

    @After
    public void tearDown() {

    }

    @AfterClass
    public static void close() {}

    //Test Methods
    @Test
    public void shouldHashString() {
        String testString = "test-this-string-for-hash";
        String expectedHash = "$2y$05$m0HraIJOf/530zwhBkc1p.YSvv4Mi6LeMDfp.7Rw8UlNjB3VCg6GG";
        String actualHash = BCryptHasher.hashString(testString);
        Assert.assertEquals(actualHash, expectedHash);
    }

    @Test
    public void shouldReturnEmployee() {
        EmployeeRepo er = new EmployeeRepo();

        String testEmail = "wubbalubba@dub.dub";
        String expectedFirstName = "Rick";
        String actualFirstName = er.getEmployeeByEmail(testEmail).getFirstName();
        Assert.assertEquals(actualFirstName, expectedFirstName);
    }

    @Test
    public void shouldValidateEmployee() {
        EmployeeRepo er = new EmployeeRepo();

        String testEmail = "wubbalubba@dub.dub";
        String testPass = "morty$uX";
        String expectedFirstName = "Rick";
        String actualFirstName = er.validateLogin(testEmail, testPass).getFirstName();
        Assert.assertEquals(actualFirstName, expectedFirstName);
    }

    @Test
    public void shouldReturnManagers() {
        EmployeeRepo er = new EmployeeRepo();

        String expectedFirstName = "Hayden";
        String actualFirstName = er.getEmployeesByAdminLvl(1).get(0).getFirstName();
        Assert.assertEquals(actualFirstName, expectedFirstName);
    }

    @Test
    public void shouldReturnEmployeeRequests() {
        ReimbursementRepo rr = new ReimbursementRepo();
        int testId = 101;
        List<Reimbursement> actualArr = rr.getEmployeeRequests(testId);
        Assert.assertEquals(testId, actualArr.get(0).getEmployee());
    }

    @Test
    public void shouldReturnRequests() {
        ReimbursementRepo rr = new ReimbursementRepo();
        List<Reimbursement> actualArr = rr.getRequests("PENDNG");
        Assert.assertTrue(actualArr.size() > 0);
    }

//    @Test
//    public void shouldLogin() {
//        Context context = PowerMockito.mock(Context.class);
//        PowerMockito.when(context.formParam("username")).thenReturn("javier.gonzalez@revature.net");
//        PowerMockito.when(context.formParam("password")).thenReturn("javiergonzalez");
//
//        LoginController.login(context);
//    }
//
//    @Test
//    public void shouldRetrieveEmployees() {
//        Context context = mock(Context.class);
//        Mockito.when(context.sessionAttribute("user")).thenReturn(new Employee(999, "Billy", "Bob", "bb@aol.com", "0", 0));
//        EmployeeController.renderEmployeeDash(context);
//        Mockito.verify(context).redirect("/employee");
//    }
}