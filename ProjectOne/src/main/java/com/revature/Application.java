package com.revature;

import com.revature.controllers.EmployeeController;
import com.revature.controllers.LoginController;
import com.revature.controllers.ReimbursementController;
import io.javalin.Javalin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Main Application class. Houses management code for the whole application.
 *
 * @author Javier Gonzalez
 */
public class Application {

    /** Logger object */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {

        Javalin app = Javalin
                .create(config -> {
                    config.addStaticFiles("/public/");
                    config.contextPath = "/app";
                }).start(8080);

        //Handlers
        app.post("/login", LoginController::login);
        app.get("/logout", LoginController::logout);
        app.get("/employee", EmployeeController::renderEmployeeDash);
        app.get("/reimbursements", ReimbursementController::viewRequests);
        app.post("/request-submit", ReimbursementController::submitRequest);
        app.get("/viewEmployees", EmployeeController::retrieveEmployees);
        app.get("/viewReimbursements", ReimbursementController::viewResolvedRequests);
        app.get("/pendingReimbursements", ReimbursementController::viewPendingRequests);
        app.post("/registerEmployee", EmployeeController::registerNewEmployee);
    }
}
