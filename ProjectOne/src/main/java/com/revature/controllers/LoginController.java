package com.revature.controllers;

import com.revature.Application;
import com.revature.data.EmployeeRepo;
import com.revature.models.Employee;
import io.javalin.http.Context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class LoginController {

    /** Logger object */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    public static void login(Context ctx) {
        lg.atDebug().log("Login request");
        EmployeeRepo er = new EmployeeRepo();
        Employee employee = er.validateLogin(ctx.formParam("username"), ctx.formParam("password"));
        lg.atDebug().log(employee);
        if(employee != null) {
            lg.atInfo().log("Login successful!");
            lg.atDebug().log("Admin Level: " + employee.getAdminLevel());
            ctx.sessionAttribute("user", employee);
            if(employee.getAdminLevel() == 0) {
                ctx.redirect("/app/dashboard-employee.html");
            } else if (employee.getAdminLevel() == 1) {
                ctx.redirect("/app/dashboard-manager.html");
            }
        } else {
            lg.atWarn().log("No employee found with entered credentials!");
            ctx.redirect("/app");
        }
    }

    public static void logout(@NotNull Context ctx) {
        lg.atDebug().log("Logout request");
        ctx.sessionAttribute("user", null);
        ctx.redirect("/app");
    }
//    public static Handler beforeLogin = ctx -> {
//        if(ctx.sessionAttribute("user") == null)
//    };

}
