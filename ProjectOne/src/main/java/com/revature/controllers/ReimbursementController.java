package com.revature.controllers;

import com.revature.Application;
import com.revature.data.ReimbursementRepo;
import com.revature.models.Employee;
import com.revature.models.Reimbursement;
import com.revature.models.Response;
import io.javalin.http.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReimbursementController {

    /** Logger object */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    public static void submitRequest(@NotNull Context ctx) {
        Reimbursement reimbursement = new Reimbursement();
        ReimbursementRepo rr = new ReimbursementRepo();
        Employee e = ctx.sessionAttribute("user");
        reimbursement.setEmployee(e.getId());
        reimbursement.setStatus("PENDNG");
        reimbursement.setRequestDate(new Timestamp(new Date().getTime()));
        try {
            reimbursement.setAmount(Long.parseLong(ctx.formParam("amount")));
        }catch(NumberFormatException err) {
            reimbursement.setAmount((long) Double.parseDouble(ctx.formParam("amount")));
        }
        reimbursement.setReason(ctx.formParam("title"));
        rr.save(reimbursement);
        ctx.redirect("dashboard-employee.html");
    }

    public static void viewRequests(@NotNull Context ctx) {
        lg.atDebug().log("view requests reached!");
        Response r = new Response();
        ReimbursementRepo rr = new ReimbursementRepo();
        Employee e = ctx.sessionAttribute("user");

        r.setBody(rr.getEmployeeRequests(e.getId()));
        ctx.json(r);
    }
    public static void viewPendingRequests(@NotNull Context ctx) {
        lg.atDebug().log("view requests reached!");
        Response r = new Response();
        ReimbursementRepo rr = new ReimbursementRepo();

        r.setBody(rr.getRequests("PENDNG"));
        ctx.json(r);
    }
    public static void viewResolvedRequests(@NotNull Context ctx) {
        lg.atDebug().log("view resolved requests reached!");
        Response r = new Response();
        ReimbursementRepo rr = new ReimbursementRepo();
        Employee e = ctx.sessionAttribute("user");

        List<Reimbursement> respList = new ArrayList<Reimbursement>();

        respList.addAll(rr.getRequests("PENDNG"));
        respList.addAll(rr.getRequests("APPROV"));
        respList.addAll(rr.getRequests("DENIED"));
        lg.atDebug().log(respList.get(0));
        r.setBody(respList);
        ctx.json(r);
    }

}
