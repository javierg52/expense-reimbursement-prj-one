package com.revature.controllers;

import com.revature.Application;
import com.revature.data.EmployeeRepo;
import com.revature.models.Employee;
import com.revature.models.Response;
import io.javalin.http.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class EmployeeController {

    /** Logger object */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    public static void renderEmployeeDash(@NotNull Context ctx) {
        Response r = new Response();
        r.setBody(ctx.sessionAttribute("user"));
        ctx.json(r);
    }

    public static void retrieveEmployees(@NotNull Context ctx) {
        Response r = new Response();
        EmployeeRepo er = new EmployeeRepo();
        r.setBody(er.getEmployeesByAdminLvl(0));
        ctx.json(r);
    }

    public static void registerNewEmployee(@NotNull Context ctx) {
        EmployeeRepo er = new EmployeeRepo();
        Employee newEmployee = new Employee();
        newEmployee.setFirstName(ctx.formParam("fname"));
        newEmployee.setLastName(ctx.formParam("lname"));
        newEmployee.setEmail(ctx.formParam("email"));
        newEmployee.setPassHash("password");
        newEmployee.setAdminLevel(Integer.parseInt(ctx.formParam("level")));
        er.save(newEmployee);
        ctx.redirect("/app/dashboard-manager.html");
    }
}
