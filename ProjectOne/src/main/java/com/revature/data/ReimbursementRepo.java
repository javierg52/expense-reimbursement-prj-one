package com.revature.data;

import com.revature.models.Employee;
import com.revature.models.Reimbursement;
import com.revature.utilities.SessionConfig;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class ReimbursementRepo implements Repository<Reimbursement>{

    SessionFactory sf;
    SessionConfig sc;

    public ReimbursementRepo() {
        sc = new SessionConfig();
        sc.initSessionFactoryFromEnv();
        sf = sc.getSf();
    }

    @Override //Can be used to submit - and edit - requests
    public void save(Reimbursement r) {
        Session session = sf.openSession();
        session.beginTransaction();
        session.saveOrUpdate(r);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void deleteById(int id) {
        Session session = sf.openSession();
        Transaction t = session.beginTransaction();
        Reimbursement result = (Reimbursement) session.createCriteria(Reimbursement.class)
                .add(Restrictions.idEq(id))
                .uniqueResult();

        if (result != null) {
            session.delete(result);
            t.commit();
        }

        session.close();
    }
    //get requests to populate tables
    public List<Reimbursement> getEmployeeRequests(int id) {
        Session session = sf.openSession();
        Query nq = session.getNamedQuery("getRequestByEmployee");
        nq.setInteger("employee", id);

        List<Reimbursement> requests = nq.list();
        session.close();
        return requests;
    }

    //get requests to populate tables
    public List<Reimbursement> getRequests(String status) {
        Session session = sf.openSession();
        Query nq = session.getNamedQuery("getRequestByStatus");
        nq.setString("status", status);

        List<Reimbursement> requests = nq.list();
        session.close();
        return requests;
    }
}
