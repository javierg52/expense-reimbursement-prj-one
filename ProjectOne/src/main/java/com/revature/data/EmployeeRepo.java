package com.revature.data;

import com.revature.Application;
import com.revature.models.Employee;

import com.revature.utilities.BCryptHasher;
import com.revature.utilities.SessionConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class EmployeeRepo implements Repository<Employee>{

    /** Logger object */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    SessionFactory sf;
    SessionConfig sc;

    public EmployeeRepo() {
        sc = new SessionConfig();
        sc.initSessionFactoryFromEnv();
        sf = sc.getSf();
    }

    @Override //can be used to register - and update the info of - Employees
    public void save(Employee e) {
        Session session = sf.openSession();
        session.beginTransaction();
        session.saveOrUpdate(e);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void deleteById(int id) {
        Session session = sf.openSession();
        Transaction t = session.beginTransaction();
        Employee result = (Employee) session.createCriteria(Employee.class)
                .add(Restrictions.idEq(id))
                .uniqueResult();

        if (result != null) {
            session.delete(result);
            t.commit();
        }

        session.close();
    }

    public Employee getEmployeeByEmail(String email) {
        Session session = sf.openSession();
        Query nq = session.getNamedQuery("getEmployeeByEmail");
        nq.setString("email", email);

        Employee result = (Employee) nq.list().get(0);
        session.close();
        return result;
    }

    public Employee validateLogin(String email, String password) {
        Employee employee = getEmployeeByEmail(email);

        if(employee == null) {
            lg.atError().log("No user with that email address.");
        } else {
            if(employee.getPassHash().equals(BCryptHasher.hashString(password))) {
                //Iff username and password can be validated, return employee
                return employee;
            } else {
                lg.atError().log("Incorrect password.");
            }
        }
        //If user cannot be validated, return null
        return null;
    }

    public List<Employee> getEmployeesByAdminLvl(int adminLvl) {
        Session session = sf.openSession();
        Query nq = session.getNamedQuery("getEmployeeByAdminLvl");
        nq.setInteger("adminLevel", adminLvl);

        List<Employee> employees = nq.list();
        session.close();
        return employees;
    }
}
