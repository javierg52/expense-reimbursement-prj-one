package com.revature.data;

import org.hibernate.SessionFactory;

/**
 *<h2>Repository</h2>
 * <p>Interface</p>
 *
 * @author Javier Gonzalez
 *
 * @setSessionFactory
 *
 * @save
 * @deleteById
 */
public interface Repository<T> {

    //CRUD functions
    void save(T model);
    void deleteById(int id);
}
