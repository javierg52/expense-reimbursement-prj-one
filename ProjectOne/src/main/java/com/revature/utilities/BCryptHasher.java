package com.revature.utilities;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;
import at.favre.lib.crypto.bcrypt.BCrypt.Result;
import at.favre.lib.crypto.bcrypt.BCrypt.Version;
import java.security.SecureRandom;

import com.revature.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 *
 * @author Javier Gonzalez
 */
public class BCryptHasher {
    /**
     * Logger object
     */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());

    //Static string hashing method
    public static String hashString(String stringToHash) {
        lg.atDebug().log("HASH INPUT: " + stringToHash);
        String bcryptHashString = BCrypt.with(Version.VERSION_2Y, new SecureRandom(new byte[]{5, 2, 8, 4, 3, 7}), LongPasswordStrategies.strict(Version.VERSION_2Y)).hashToString(5, stringToHash.toCharArray());
        Result result = BCrypt.verifyer().verify(stringToHash.toCharArray(), bcryptHashString);
        if (result.verified) {
            lg.atDebug().log("HASH RESULT: " + bcryptHashString);
            return bcryptHashString;
        } else {
            lg.atError().log("String could not be hashed and verified!");
            return null;
        }
    }
}
