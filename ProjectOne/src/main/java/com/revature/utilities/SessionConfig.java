package com.revature.utilities;

import com.revature.Application;
import com.revature.models.Employee;
import com.revature.models.Reimbursement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class SessionConfig {

    /**
     * Logger object
     */
    public static final Logger lg = LogManager.getLogger(Application.class.getName());
    private SessionFactory sf;

    public void configure(Properties p) {
        Configuration config = null;
        if(p == null) {
            config = new Configuration().configure();
        }else {
            config = new Configuration();
            config.setProperties(p);
            config.addAnnotatedClass(Employee.class);
            config.addAnnotatedClass(Reimbursement.class);
        }
        if(config != null) {
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(config.getProperties());
            this.sf = config.buildSessionFactory(builder.build());
        }
    }

    public void initSessionFactoryFromEnv() {
        Properties dbp = new Properties();
        try{
            dbp.load(new FileReader(Objects.requireNonNull(ClassLoader.getSystemClassLoader().getResource("db.properties")).getFile()));
        } catch(IOException e) {
            lg.atDebug().log(e.getMessage());
        }

        String url = dbp.getProperty("db.url");
        String user = dbp.getProperty("db.username");
        String pass = dbp.getProperty("db.password");
        dbp.setProperty("hibernate.connection.url", url);
        dbp.setProperty("hibernate.connection.username", user);
        dbp.setProperty("hibernate.connection.password", pass);
        dbp.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        dbp.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

        configure(dbp);
    }

    public SessionFactory getSf() {
        return sf;
    }
}