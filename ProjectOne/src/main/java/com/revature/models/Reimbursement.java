package com.revature.models;

import javax.persistence.*;
import java.sql.Timestamp;

@NamedQueries(
        value={
                @NamedQuery(
                        name="getSomeRequestByEmployee",
                        query="from Reimbursement where employee = :employee and status = :status"
                ),
                @NamedQuery(
                        name="getRequestByEmployee",
                        query="from Reimbursement where employee = :employee"
                ),
                @NamedQuery(
                        name="getRequestByStatus",
                        query="from Reimbursement where status = :status"
                )
        }
)
@Entity
@Table(name="REQUEST")
public class Reimbursement {

    //Columns
    @Id
    @Column(name="REQUEST_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="EMPLOYEE_ID")
    private int employee;

    @Column(name="STATUS")
    private String status;

    @Column(name="REASON")
    private String reason;

    @Column(name="AMOUNT")
    private Long amount;

    @Column(name="REQUEST_DATE")
    private Timestamp requestDate;

    @Column(name="APPROVED_BY")
    private int manager;

    //Constructor
    public Reimbursement() {}

    public Reimbursement(int id, int employee, String status, String reason, Long amount, Timestamp requestDate, int manager) {
        this.id = id;
        this.employee = employee;
        this.status = status;
        this.reason = reason;
        this.amount = amount;
        this.requestDate = requestDate;
        this.manager = manager;
    }

    //Getters & Setters
    public int getId() {
        return id;
    }

    public int getEmployee() {
        return employee;
    }
    public void setEmployee(int employee) {
        this.employee = employee;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getAmount() {
        return amount;
    }
    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }
    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public int getManager() {
        return manager;
    }
    public void setManager(int manager) {
        this.manager = manager;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", employee=" + employee +
                ", status='" + status + '\'' +
                ", requestDate='" + requestDate + '\'' +
                ", manager=" + manager +
                '}';
    }
}
