package com.revature.models;

import javax.persistence.*;

@NamedQueries(
        value={
                @NamedQuery(
                        name="getEmployeeByEmail",
                        query="from Employee where email = :email"
                ),
                @NamedQuery(
                        name="getEmployeeByAdminLvl",
                        query="from Employee where adminLevel = :adminLevel"
                )
        }
)
@Entity
@Table(name="EMPLOYEE")
public class Employee {

    //Table Primary Key
    @Id
    @Column(name="EMPLOYEE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //Employee name
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="LAST_NAME")
    private String lastName;

    //Login information
    @Column(name="EMAIL")
    private String email;
    @Column(name="PASS_HASH")
    private String passHash;

    //Other Information
    @Column(name="ADMIN_LEVEL")
    private int adminLevel;

    //Constructors
    public Employee(String firstName, String lastName, String email, String passHash, int adminLevel) {
        this.id = null;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.passHash = passHash;
        this.adminLevel = adminLevel;
    }

    public Employee(int id, String firstName, String lastName, String email, String passHash, int adminLevel) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.passHash = passHash;
        this.adminLevel = adminLevel;
    }

    public Employee() {

    }

    //Getters & Setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassHash() {
        return passHash;
    }
    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public int getAdminLevel() {
        return adminLevel;
    }
    public void setAdminLevel(int adminLevel) {
        this.adminLevel = adminLevel;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", passHash='" + passHash + '\'' +
                ", adminLevel=" + adminLevel +
                '}';
    }
}
